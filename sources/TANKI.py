# Créé par SURACE, le 12/03/2024 en Python 3.7
# Créé par serba, le 10/03/2024 en Python 3.7

import pygame
import sys
import math
import random

## Initialisation de Pygame
pygame.init()

## Paramètres de l'écran
screen_info = pygame.display.Info()
largeur = screen_info.current_w
hauteur = screen_info.current_h

ecran = pygame.display.set_mode((largeur, hauteur), pygame.FULLSCREEN)
pygame.display.set_caption("Tanki Menu")

## Couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)
VERT = (0, 255, 0)
BLEU = (0, 0, 255)
##vert = (51,255,255)
vert = (255,102,204)

## Police d'écriture
police_titre = pygame.font.Font(None, 140)
police_bouton = pygame.font.Font(None, 36)

## Fonction pour afficher le texte
def afficher_texte(texte, police, couleur, x, y):
    texte_surface = police.render(texte, True, couleur)
    texte_rect = texte_surface.get_rect(center=(x, y))
    ecran.blit(texte_surface, texte_rect)

def rect_boutons(tank,largeur,hauteur):
    rect = tank.get_rect()
    rect.center = largeur, hauteur
    return rect

## Importation des images des boutons
tanki = pygame.image.load("tanki.png")
tanki = pygame.transform.scale(tanki,(400,100))
rect_tanki = rect_boutons(tanki,largeur // 2, hauteur // 4)

play = pygame.image.load("play.png")
play = pygame.transform.scale(play,(200,50))
rect_play = rect_boutons(play,largeur/2, 375)

skin_bouton = pygame.image.load("skin.png")
skin_bouton = pygame.transform.scale(skin_bouton,(200,50))
rect_skin = rect_boutons(skin_bouton,largeur/2, 475)

skin_titre = pygame.image.load("skin.png")
skin_titre = pygame.transform.scale(skin_titre,(400,100))
rect_skin_titre = rect_boutons(skin_titre,largeur/2, 275)

maps = pygame.image.load("maps.png")
maps = pygame.transform.scale(maps,(200,50))
rect_maps = rect_boutons(maps,largeur/2, 575)

quit = pygame.image.load("quit.png")
quit = pygame.transform.scale(quit,(200,50))
rect_quit = rect_boutons(quit,largeur/2, 675)

fond_ecran = pygame.image.load("fond_ecran1.png")
fond_ecran = pygame.transform.scale(fond_ecran,(500,500))

##---------------------------------------------
tank_jeu = "nul"
ennemis = []
degats = 25
nb_ennemis = 8

## Boucle principale
while True:
    ecran.fill(NOIR)

    ## Titre
    ecran.blit(tanki,rect_tanki)

    ## Gestion des événements
    for evenement in pygame.event.get():

        if evenement.type == pygame.MOUSEBUTTONDOWN:
            x, y = pygame.mouse.get_pos()
            ## Vérification du clic sur les boutons
            if largeur/2-100 <= x <=largeur/2+100 and 450 <= y <= 500:
                print("Bouton Skin cliqué")
                #------------------Ecran-------------------

                ## Paramètres de l'écran
                screen_info = pygame.display.Info()
                largeur = screen_info.current_w
                hauteur = screen_info.current_h

                ecran = pygame.display.set_mode((largeur, hauteur), pygame.FULLSCREEN)
                pygame.display.set_caption("Tanki_Menu_skin")

                couleur = vert
                active = False

                ## Police d'écriture
                police_ecriture = pygame.font.Font(None, 32)

                #------------------fonctions-------------------

                ## Fonction pour afficher le texte
                def afficher_texte(texte, police, couleur, x, y):
                    texte_surface = police.render(texte, True, couleur)
                    texte_rect = texte_surface.get_rect(center=(x, y))
                    ecran.blit(texte_surface, texte_rect)

                ## Fonction pour ouvrir les images des tanks
                def ouvrir_tank(nom_fic):
                    tank = pygame.image.load(nom_fic)
                    tank.convert()
                    tank = pygame.transform.rotate(tank,90)
                    return tank

                ## Fonction pour donner les coordonnées des images des tanks
                def rect_tank(tank,largeur,hauteur):
                    rect = tank.get_rect()
                    rect.center = largeur, hauteur
                    return rect


                #------------------images tanks-------------------

                ##ouvrir image tank nul
                tank_nul = ouvrir_tank('classe nul assemblé jeu.png')
                rect_nul = rect_tank(tank_nul,largeur//5, hauteur//2)


                ##ouvrir image tank vitesse
                tank_vit = ouvrir_tank('classe vitesse assemblé jeu.png')
                rect_vit = rect_tank(tank_vit,largeur//2, hauteur//1.25)

                ##ouvrir image tank degats
                tank_deg = ouvrir_tank('classe degats assemblé jeu.png')
                rect_deg = rect_tank(tank_deg,largeur//1.2, hauteur//2)

                ##ouvrir image tank secret
                tank_sec = ouvrir_tank('classe secret assemblé jeu.png')
                ##tank_sec = pygame.transform.scale(tank_sec,(1200,1200))

                ##tank choisi
                tank_choisi = tank_nul
                rect_choisi = rect_tank(tank_choisi,largeur//2, hauteur//2)

                #------------------input text-------------------

                ##mot clé
                mot_clé = ''

                ##coordonées et taille de la box
                input_rect = pygame.Rect(largeur-140,hauteur-60,140,32)

                #------------------Boucle principale-------------------
                skin = True
                while skin:
                    ecran.fill(NOIR)

                    ## Titre
                    ecran.blit(skin_titre,rect_skin_titre)

                    ## Gestion des événements
                    for evenement in pygame.event.get():
                        if evenement.type == pygame.QUIT:
                            pygame.quit()
                            sys.exit()
                        elif evenement.type == pygame.MOUSEBUTTONDOWN:
                            x, y = pygame.mouse.get_pos()
                            ## Vérification du clic sur les boutons
                            if largeur//5-175 <= x <= largeur//5+175 and hauteur//2-117 <= y <= hauteur//2+117:
                                tank_choisi = tank_nul
                                tank_jeu = "nul"
                                vitesse=2
                                degats=25


                            elif largeur//2-175 <= x <= largeur//2+215 and hauteur//1.25-130 <= y <= hauteur//1.25+130:
                                tank_choisi = tank_vit
                                tank_jeu = "vitesse"
                                vitesse=3
                                degats=20

                            elif largeur//1.2-175 <= x <= largeur//1.2+155 and hauteur//2-130 <= y <= hauteur//2+130:
                                tank_choisi = tank_deg
                                tank_jeu = "degats"
                                vitesse=1
                                degats=34

                        elif evenement.type == pygame.KEYDOWN:

                            if evenement.key == pygame.K_ESCAPE:
                                skin = False


                #------------------Affichage-------------------


                    ## Affichage des boutons
                    pygame.draw.rect(ecran, vert, (largeur//5-175, hauteur//2-117,350,234),2)
                    pygame.draw.rect(ecran, vert, (largeur//2-175, hauteur//1.25-130,390,260),2)
                    pygame.draw.rect(ecran, vert, (largeur//1.2-175, hauteur//2-130,330,260),2)

                    ##Affichage des tanks
                    ecran.blit(tank_nul,rect_nul)

                    ecran.blit(tank_vit, rect_vit)

                    ecran.blit(tank_deg, rect_deg)

                    ecran.blit(tank_choisi, rect_choisi)

                    ## Mise à jour de l'écran
                    pygame.display.flip()


            elif largeur/2 - 100 <= x <= largeur/2 + 100 and 350 <= y <= 400:
                print("Bouton Jouer cliqué")
                ## Ajoutez ici le code à exécuter lorsque le bouton Jouer est cliqué
                screen_info = pygame.display.Info()
                largeur = screen_info.current_w
                hauteur = screen_info.current_h

                ecran = pygame.display.set_mode((largeur, hauteur), pygame.FULLSCREEN)
                pygame.display.set_caption("Tanki Game")

                x = largeur/2
                y = hauteur/2


                angle = 0
                vie_joueur = 100
                vie_ennemis = [100]
                balles = []


                class Balle:
                    def __init__(self, x, y, angle, proprietaire):
                        self.x = x
                        self.y = y
                        self.vitesse = -10
                        if proprietaire == "joueur":
                            self.surface = pygame.Surface((10, 10), pygame.SRCALPHA)
                            self.angle = angle + random.randint(-2,2)
                            pygame.draw.circle(self.surface, BLEU, (5, 5), 5)
                        elif proprietaire == "ennemi":
                            self.surface = pygame.Surface((10, 10), pygame.SRCALPHA)
                            pygame.draw.circle(self.surface, BLEU, (5, 5), 5)
                            self.angle = angle + random.randint(-5,5)
                        pygame.draw.circle(self.surface, BLANC, (5, 5), 5)
                        self.mask = pygame.mask.from_surface(self.surface)  # Surface de masque de collision
                        self.proprietaire = proprietaire

                    def deplacer(self):
                        self.x += self.vitesse * math.sin(math.radians(self.angle))
                        self.y -= self.vitesse * math.cos(math.radians(self.angle))



                def ouvrir_tank(nom_fic):
                    tank = pygame.image.load(nom_fic)
                    tank.convert()
                    return tank

                def afficher_tank(x, y, angle, angle_canon):

                    tank = ouvrir_tank('corp classe '+ tank_jeu +' jeu.png')

                    canon = ouvrir_tank('canon classe '+ tank_jeu +' jeu.png')
                    canon = pygame.transform.rotate(canon,180)

                    tank_rotate = pygame.transform.rotate(tank, -angle)
                    canon_rotate = pygame.transform.rotate(canon, -angle_canon)

                    tank_rect = tank_rotate.get_rect(center=(x, y))

                    # Position du centre du canon relativement au centre du tank
                    canon_offset_x = 0
                    canon_offset_y = -tank.get_height() / 2 + 22.5

                    # Position du bout du canon relativement au centre du tank
                    bout_canon_x = x + canon_offset_x
                    bout_canon_y = y + canon_offset_y

                    # Position du centre du canon dans le référentiel du char
                    canon_center = (x + canon_offset_x, y + canon_offset_y)

                    canon_rect = canon_rotate.get_rect(center=canon_center)

                    return tank_rotate, canon_rotate, tank_rect, canon_rect  # Retourne les surfaces et les rectangles

                class Ennemi:
                    def __init__(self, x, y):
                        self.x = x
                        self.y = y
                        self.vitesse = 0
                        self.angle = 0
                        self.temps_entre_tirs = random.randint(60, 180)  # Délai aléatoire pour le tir
                        self.temps_avant_prochain_tir = self.temps_entre_tirs
                        self.largeur = 30
                        self.hauteur = 45
                        self.vie = 100

                    def afficher_ennemi(self, tank_x, tank_y):
                        ennemi = ouvrir_tank('corp classe ennemi jeu 2.png')

                        canon = ouvrir_tank('canon classe ennemi jeu.png')
                        canon = pygame.transform.rotate(canon,180)

                        angle_to_tank = math.degrees(math.atan2(tank_y - self.y, tank_x - self.x)) - 90
                        ennemi_rotate = pygame.transform.rotate(ennemi, -self.angle)
                        canon_rotate = pygame.transform.rotate(canon, -angle_to_tank)

                        ennemi_rect = ennemi_rotate.get_rect(center=(self.x, self.y))

                        # Position du centre du canon relativement au centre de l'ennemi
                        canon_offset_x = 0
                        canon_offset_y = -ennemi.get_height() / 2 + 15

                        # Position du centre du canon dans le référentiel de l'ennemi
                        canon_center = (self.x + canon_offset_x, self.y + canon_offset_y)

                        canon_rect = canon_rotate.get_rect(center=canon_center)

                        return ennemi_rotate, canon_rotate, ennemi_rect, canon_rect

                    def tirer(self, tank_x, tank_y, proprietaire_balle):
                        # Calculer la position de la balle en fonction du bout du canon de l'ennemi
                        bullet_x = self.x + math.cos(math.radians(self.angle))
                        bullet_y = self.y + math.sin(math.radians(self.angle))

                        # Calculer l'angle pour que la balle se dirige vers le tank
                        angle_to_tank = math.atan2(tank_y - bullet_y, tank_x - bullet_x)
                        angle_to_tank = math.degrees(angle_to_tank) - 90

                        # Créer une nouvelle balle pour cet ennemi avec l'angle vers le tank
                        nouvelle_balle = Balle(bullet_x, bullet_y, angle_to_tank, proprietaire_balle)
                        balles.append(nouvelle_balle)  # Ajout de la balle à la liste des balles
                # Liste pour stocker les ennemis
                ennemis = []

                # Fonction pour générer un ennemi aléatoire
                def generer_ennemi():
                    if len(ennemis) < 5:  # Vous pouvez ajuster le nombre d'ennemis
                        ennemi_x = random.randint(100, largeur - 100)
                        ennemi_y = random.randint(100, hauteur - 100)
                        nouvel_ennemi = Ennemi(ennemi_x, ennemi_y)
                        ennemis.append(nouvel_ennemi)

                continuer = True
                horloge = pygame.time.Clock()

                vitesse_rotation_canon = 2.0  # Vitesse de rotation du canon
                angle_canon = 0  # Initialisez l'angle du canon
                ennemis_generes = False

                while continuer:
                    ecran.fill(NOIR)
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                            continuer = False
                        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:  # clique gauche
                            # calcul la position de la balle en fonction du bout du canon
                            bullet_x = x + math.cos(math.radians(angle_canon + 90)) * 40  # ajuste la position de balle initialement x position
                            bullet_y = y + math.sin(math.radians(angle_canon + 90)) * 40  # ajuste la position de balle initialement y position
                            proprietaire_balle = "joueur"
                            new_bullet = Balle(bullet_x, bullet_y, angle_canon, proprietaire_balle)  # Creation d'une nouvelle balle
                            balles.append(new_bullet)

                    mouse_x, mouse_y = pygame.mouse.get_pos()
                    angle_canon_cible = math.degrees(math.atan2(mouse_y - y, mouse_x - x)) - 90

                    if angle_canon != angle_canon_cible:
                        delta_angle = angle_canon_cible - angle_canon
                        if delta_angle > 180:
                            delta_angle -= 360
                        elif delta_angle < -180:
                            delta_angle += 360

                        signe_rotation = 1 if delta_angle > 0 else -1
                        rotation_effective = min(vitesse_rotation_canon, abs(delta_angle))

                        angle_canon += signe_rotation * rotation_effective


                    # Déplacement du tank
                    touches = pygame.key.get_pressed()
                    if touches[pygame.K_d]:
                        angle += vitesse
                    elif touches[pygame.K_a]:
                        angle -= vitesse

                    if touches[pygame.K_z]:
                        new_y = y - vitesse * math.cos(math.radians(angle))
                        new_x = x + vitesse * math.sin(math.radians(angle))
                        # Vérification des limites de l'écran pour le déplacement vertical
                        if 0 <= new_x <= largeur and 0 <= new_y <= hauteur:
                            y = new_y
                            x = new_x
                    elif touches[pygame.K_s]:
                        new_y = y + vitesse * math.cos(math.radians(angle))
                        new_x = x - vitesse * math.sin(math.radians(angle))
                        # Vérification des limites de l'écran pour le déplacement vertical
                        if 0 <= new_x <= largeur and 0 <= new_y <= hauteur:
                            y = new_y
                            x = new_x

                    # Vérification des limites de l'écran pour le déplacement horizontal
                    if touches[pygame.K_q]:
                        angle -= vitesse
                    elif touches[pygame.K_d]:
                        angle += vitesse - 2

                    tank_surface, canon_surface, tank_rect, canon_rect = afficher_tank(x, y, angle, angle_canon)
                    ecran.blit(tank_surface, tank_rect)
                    ecran.blit(canon_surface, canon_rect)

                    if not ennemis_generes:
                        for _ in range(5):  # Vous pouvez ajuster le nombre d'ennemis
                            generer_ennemi()
                        ennemis_generes = True

                    for ennemi in ennemis:
                        ennemi_surface, ennemi_canon_surface, ennemi_rect, ennemi_canon_rect = ennemi.afficher_ennemi(x, y)  # Passer les coordonnées du tank
                        ecran.blit(ennemi_surface, ennemi_rect)
                        ecran.blit(ennemi_canon_surface, ennemi_canon_rect)

                        ennemi.temps_avant_prochain_tir -= 1
                        if ennemi.temps_avant_prochain_tir <= 0:
                            ennemi.tirer(x, y,"ennemi")  # Passer les coordonnées du tank à la méthode tirer()
                            ennemi.temps_entre_tirs = random.randint(180, 300)
                            ennemi.temps_avant_prochain_tir = ennemi.temps_entre_tirs

                    for balle in balles:
                        balle.deplacer()
                        pygame.draw.circle(ecran, BLEU, (int(balle.x), int(balle.y)), 5)
                        if not (0 <= balle.x <= largeur and 0 <= balle.y <= hauteur):
                            balles.remove(balle)
                        else:
                            if balle.proprietaire == "joueur":
                                for ennemi in ennemis :
                                    ennemi_rect = pygame.Rect(ennemi.x-15, ennemi.y-15, ennemi.largeur, ennemi.hauteur)
                                    balle_rect = pygame.Rect(balle.x - 5, balle.y - 5, 10, 10)  # Utilisez la taille de votre balle
                                    if balle_rect.colliderect(ennemi_rect):
                                        balles.remove(balle)
                                        ennemi.vie -= degats  # Réduire la vie de l'ennemi de 25

                                        if ennemi.vie <= 0:
                                            ennemis.remove(ennemi)


                                        break  # Arrête la boucle après avoir trouvé l'ennemi touché

                            elif balle.proprietaire == "ennemi":
                                balle_rect = pygame.Rect(balle.x - 5, balle.y - 5, 10, 10)  # Créer un rectangle autour de la balle ennemie
                                joueur_rect = pygame.Rect(x - 15, y - 25, 30, 50)  # Créer un rectangle autour du joueur (30 de largeur, 50 de hauteur)
                                if balle_rect.colliderect(joueur_rect):  # Vérifier s'il y a une collision entre la balle ennemie et le joueur
                                    vie_joueur -= 20  # Diminuer la vie du joueur
                                    balles.remove(balle)
                                    if vie_joueur == 0:
                                        inDeath=True
                                        pygame.draw.rect(ecran, vert, (largeur/2-100, 150, 200, 50))
                                        afficher_texte("Retour au menu", police_bouton, NOIR, largeur/2, 175)
                                        afficher_texte("Vous êtes mort(e).", police_bouton, vert, largeur/2, 675)
                                        pygame.display.flip()
                                        while inDeath:
                                            for evenement in pygame.event.get():
                                                if evenement.type == pygame.MOUSEBUTTONDOWN:
                                                    x, y = pygame.mouse.get_pos()
                                                    if largeur/2 - 100 <= x <= largeur/2 + 100 and 150 <= y <= 200:
                                                        inDeath=False
                                                        continuer=False

                    if nb_ennemis == 0:
                        continuer = False
                    pygame.display.flip()
                    horloge.tick(60)

                ## Ajoutez le code à exécuter lorsque le bouton Skin est cliqué
            elif largeur/2-100 <= x <=largeur/2+100  and 550 <= y <= 600:
                print("Bouton Quitter cliqué")
                pygame.quit()
                sys.exit()
                ## Ajoutez le code à exécuter lorsque le bouton Carte est cliqué


    ## Affichage des boutons
    ecran.blit(fond_ecran,(400,550))

    ecran.blit(play,rect_play)

    ecran.blit(skin_bouton,rect_skin)

    ecran.blit(quit,rect_maps)




    ## Mise à jour de l'écran
    pygame.display.flip()


